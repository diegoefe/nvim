-- downloading and installing packer, if missing
local install_path = vim.fn.stdpath 'data' .. '/site/pack/packer/start/packer.nvim'
-- local is_bootstrap = false
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  -- is_bootstrap = true
  vim.fn.system { 'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path }
  require("diegoefe/packer").sync()
  return
else
   -- disable netrw at the very start of your init.lua (strongly advised)
   vim.g.loaded_netrw = 1
   vim.g.loaded_netrwPlugin = 1

   -- set termguicolors to enable highlight groups
   vim.opt.termguicolors = true

   -- nvim-tree setup
   require("nvim-tree").setup({
     sort_by = "case_sensitive",
     view = {
       adaptive_size = true,
       mappings = {
         list = {
           { key = "u", action = "dir_up" },
         },
       },
     },
     renderer = {
       group_empty = true,
     },
     -- filters = { dotfiles = true, },
     git = {
        ignore = false
     },
   })
  require("diegoefe")
end

