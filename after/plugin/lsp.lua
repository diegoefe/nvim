local lsp = require('lsp-zero')

lsp.preset('recommended')

lsp.ensure_installed({
   'tsserver',
   'eslint',
   'sumneko_lua',
   'rust_analyzer',
   'clangd', -- https://clangd.llvm.org/config#files
})

lsp.setup()

require'lspconfig'.sumneko_lua.setup {
    -- ... other configs
    settings = {
        Lua = {
            diagnostics = {
                globals = { 'vim' }
            }
        }
    }
}

vim.opt.signcolumn = 'yes'

-- https://neovim.io/doc/user/diagnostic.html
vim.diagnostic.config({
  virtual_text = true,
  signs = true,
  update_in_insert = false,
  underline = true,
  severity_sort = false,
  float = true,
})

