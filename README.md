### My nvim configuration
Requires NeoVim 0.8 or greater

#### Setup
- Install necessary binaries:
    ```bash
    $ cargo install tree-sitter-cli
    $ cargo install ripgrep
    ```
- Clone and run:
    ```bash
    $ cd ~/.config
    $ git clone https://gitlab.com/diegoefe/nvim.git
    $ cd nvim
    $ nvim .
    ```


#### Tips
- Still no good markdown LSP found, on prompt, refuse to install

#### About nerd-fonts (required by most file browsers)
- Linux (*)
    - Download from https://www.nerdfonts.com/font-downloads
    - Uncompress in ~/.fonts
    - Run: fc-cache -fv 
- MacOS (*)
    - brew tap homebrew/cask-fonts
    - brew install --cask font-hack-nerd-font

(*) If necessary, right-click on terminal, select Preferences, and choose font


#### MacOS quirks
- If keyboard repeat is off (j only moves one line when holding it), enable it:
    ```bash
    $ defaults write -g ApplePressAndHoldEnabled -bool false
    ```
    Exit reminal and reboot computer
